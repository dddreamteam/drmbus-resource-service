package com.drmbus.openapi.endpoint;


import com.drmbus.innerapi.dto.UserDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@OpenAPIDefinition(
        info = @Info(
                title = "Inner User",
                version = "1.0",
                description = "User inner API"
        )
)
@RequestMapping(value = "api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserEndpoint {

    @GetMapping(value = "/{uuid}")
    UserDTO fetchByUuid (@PathVariable(value = "uuid") String uuid);

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    UserDTO create (@RequestBody UserDTO user);
}
