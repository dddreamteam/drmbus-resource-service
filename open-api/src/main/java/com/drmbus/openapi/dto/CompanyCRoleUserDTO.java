package com.drmbus.openapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCRoleUserDTO {
    private Long id; // non-useful yet
    private CompanyDTO company;
    private UserDTO uuid;
    private String cRole;

    //For creating instance
    public CompanyCRoleUserDTO(CompanyDTO company, UserDTO uuid, String cRole) {
        this.company = company;
        this.uuid = uuid;
        this.cRole = cRole;
    }
}
