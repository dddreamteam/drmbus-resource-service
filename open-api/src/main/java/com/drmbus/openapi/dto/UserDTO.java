package com.drmbus.openapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private UUID uuid; // unique user id

    private String password;
    // login
    private String email;

    private boolean enabled;

    private boolean accountNonExpired;

    private boolean credentialsNonExpired;

    private boolean accountNonLocked;

/*

    private Set<Role> roles;
*/

    //  Владелец / Owner
    private List<CompanyDTO> companies;


    private Map<Long, String> companyIdToCRoleTypeMap;

    // For Select for
    public UserDTO(UUID uuid, String email, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Map<Long, String> companyIdToCRoleTypeMap) {
        this.uuid = uuid;
        this.email = email;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
       // this.companies = companies;
        this.companyIdToCRoleTypeMap = companyIdToCRoleTypeMap;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", UserDTO.class.getSimpleName() + "[", "]")
                .add("uuid=" + uuid)
                .add("password='" + password + "'")
                .add("email='" + email + "'")
                .add("enabled=" + enabled)
                .add("accountNonExpired=" + accountNonExpired)
                .add("credentialsNonExpired=" + credentialsNonExpired)
                .add("accountNonLocked=" + accountNonLocked)
                .toString();
    }
}
