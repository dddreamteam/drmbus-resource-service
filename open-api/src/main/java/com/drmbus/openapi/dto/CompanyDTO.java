package com.drmbus.openapi.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO {

    private Long id;

    private UUID ownerUser;

    private Map<UUID, String> usersCRoleMap;

    // For creating instance
    public CompanyDTO(UUID ownerUser) {
        this.ownerUser = ownerUser;
    }
}
