package com.drmbus.innerapi.endpoint;


import com.drmbus.innerapi.dto.AddressDTO;
import com.drmbus.innerapi.dto.CompanyDetailsDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaTray;
import javax.validation.Valid;

@OpenAPIDefinition(
        info = @Info(
                title = "Address",
                version = "1.0",
                description = "Address inner API"
        )
)
@RequestMapping(value = "api/address", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AddressEndpoint {

    @PostMapping(value = "")
    @ResponseStatus(code = HttpStatus.CREATED)
    AddressDTO create(@RequestBody @Valid AddressDTO addressDTO);

    @GetMapping(value = "{companyId}")
    @ResponseStatus(code = HttpStatus.OK)
    AddressDTO fetchByCompanyId(@PathVariable(value = "companyId") Long companyId);

    @PutMapping(value = "{companyId}")
    @ResponseStatus(code = HttpStatus.OK)
    AddressDTO update(@RequestBody @Valid AddressDTO addressDTO);


}
