package com.drmbus.innerapi.endpoint;

import com.drmbus.innerapi.dto.EmailDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

@OpenAPIDefinition(
        info = @Info(
                title = "Email",
                version = "1.0",
                description = "Email inner API"
        )
)
@RequestMapping(value = "api/emails", produces = MediaType.APPLICATION_JSON_VALUE)
public interface EmailEndpoint {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    EmailDTO create(@RequestBody @Valid EmailDTO emailDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    EmailDTO fetch(@PathVariable(value = "id") Long emailId);

    @PutMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    EmailDTO update(@RequestBody @Valid EmailDTO emailDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long emailId);

}
