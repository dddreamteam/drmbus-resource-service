package com.drmbus.innerapi.endpoint;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/18/2020
 */

import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.dto.StationDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

@OpenAPIDefinition(
        info = @Info(
                title = "Station",
                version = "1.0",
                description = "Station inner API"
        )
)
@RequestMapping(value = "api/stations", produces = MediaType.APPLICATION_JSON_VALUE)
public interface StationEndpoint {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    StationDTO create(@RequestBody @Valid StationDTO stationDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    StationDTO fetch(@PathVariable(value = "id") Long stationId);

    @PutMapping/*(value = "/{id}")*/
    @ResponseStatus(code = HttpStatus.OK)
    StationDTO update(@RequestBody @Valid StationDTO stationDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long stationId);

    @GetMapping(value = "/all")
    @ResponseStatus(code = HttpStatus.OK)
    List<StationDTO> fetchAll();

}
