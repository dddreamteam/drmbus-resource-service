package com.drmbus.innerapi.endpoint;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/8/2020
 */

import com.drmbus.innerapi.dto.DRMRouteDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@OpenAPIDefinition(
        info = @Info(
                title = "DRMRoute",
                version = "1.0",
                description = "DRMRoute inner API"
        )
)
@RequestMapping(value = "api/routes", produces = MediaType.APPLICATION_JSON_VALUE)
public interface DRMRouteEndpoint {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    DRMRouteDTO create(@RequestBody @Valid DRMRouteDTO routeDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    DRMRouteDTO fetch(@PathVariable(value = "id") Long routeId);

    @PutMapping/*(value = "/{id}")*/
    @ResponseStatus(code = HttpStatus.OK)
    DRMRouteDTO update(@RequestBody @Valid DRMRouteDTO routeDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long routeId);

    @GetMapping(value = "/users/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    List<DRMRouteDTO> fetchAllRoutesByUUID(@PathVariable(value = "id") String userUUID);

}
