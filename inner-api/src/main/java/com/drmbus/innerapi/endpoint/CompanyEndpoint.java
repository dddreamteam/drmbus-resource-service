package com.drmbus.innerapi.endpoint;

import com.drmbus.innerapi.dto.CompanyDTO;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

@OpenAPIDefinition(
        info = @Info(
                title = "Company",
                version = "1.0",
                description = "Company inner API"
        )
)
@RequestMapping(value = "api/companies", produces = MediaType.APPLICATION_JSON_VALUE)
public interface CompanyEndpoint {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    CompanyDTO create(@RequestBody @Valid CompanyDTO companyDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    CompanyDTO fetch(@PathVariable(value = "id") Long id);

    @PutMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    CompanyDTO update(@RequestBody @Valid CompanyDTO companyDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long id);

    @GetMapping
    @ResponseStatus(code = HttpStatus.OK)
    List<CompanyDTO> fetchAll();

    @GetMapping(value = "/{id}/routes")
    @ResponseStatus(code = HttpStatus.OK)
    List<DRMRouteDTO> fetchAllByCompanyId(@PathVariable(value = "id") Long companyId);
}
