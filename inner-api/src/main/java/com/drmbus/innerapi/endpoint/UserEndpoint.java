package com.drmbus.innerapi.endpoint;


import com.drmbus.innerapi.dto.InnerUserDTO;

import com.drmbus.innerapi.dto.InnerUserRegistrationDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

@OpenAPIDefinition(
        info = @Info(
                title = "Inner User",
                version = "1.0",
                description = "User inner API"
        )
)
@RequestMapping(value = "api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserEndpoint {

        @PostMapping
        @ResponseStatus(code = HttpStatus.CREATED)
        InnerUserDTO registration(@RequestBody @Valid InnerUserRegistrationDTO innerUserDTO);

        @GetMapping(value = "/{uuid}")
        @ResponseStatus(code = HttpStatus.OK)
        InnerUserDTO fetch(@PathVariable(value = "uuid") String id);

        @GetMapping(value = "/user")
        @ResponseStatus(code = HttpStatus.OK)
        InnerUserDTO fetchByUsername(@RequestParam String username);

        //@PostMapping
        //@ResponseStatus(code = HttpStatus.OK)
        //InnerUserDTO update(InnerUserDTO innerUserDTO);
//
        //@DeleteMapping(value = "/{id}")
        //@ResponseStatus(code = HttpStatus.NO_CONTENT)
        //void delete(@PathVariable(value = "id") Long id);

}
