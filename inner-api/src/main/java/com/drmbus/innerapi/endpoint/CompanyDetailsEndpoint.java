package com.drmbus.innerapi.endpoint;

import com.drmbus.innerapi.dto.CompanyDetailsDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import javax.validation.Valid;


@OpenAPIDefinition(
        info = @Info(
                title = "CompanyDetails",
                version = "1.0",
                description = "CompanyDetails inner API"
        )
)
@RequestMapping(value = "api/companies", produces = MediaType.APPLICATION_JSON_VALUE)
public interface CompanyDetailsEndpoint {

    @PostMapping(value = "{companyId}/details")
    @ResponseStatus(code = HttpStatus.CREATED)
    CompanyDetailsDTO create(@RequestBody @Valid CompanyDetailsDTO companyDTO);

    @GetMapping(value = "{companyId}/details")
    @ResponseStatus(code = HttpStatus.OK)
    CompanyDetailsDTO fetchByCompanyId(@PathVariable(value = "companyId") Long companyId);

    @PutMapping(value = "{companyId}/details")
    @ResponseStatus(code = HttpStatus.OK)
    CompanyDetailsDTO update(@RequestBody @Valid CompanyDetailsDTO companyDTO);
}
