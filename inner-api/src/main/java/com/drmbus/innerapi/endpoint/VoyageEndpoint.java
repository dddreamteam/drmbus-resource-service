package com.drmbus.innerapi.endpoint;

import com.drmbus.innerapi.dto.CompanyDTO;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.dto.VoyageDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

@OpenAPIDefinition(
        info = @Info(
                title = "Inner User",
                version = "1.0",
                description = "User inner API"
        )
)
@RequestMapping(value = "api/voyages", produces = MediaType.APPLICATION_JSON_VALUE)
public interface VoyageEndpoint {
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    VoyageDTO create(@RequestBody @Valid VoyageDTO voyageDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    VoyageDTO fetch(@PathVariable(value = "id") Long id);

    @PutMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    VoyageDTO update(@RequestBody @Valid VoyageDTO voyageDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long id);

    @GetMapping(value = "/users/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    List<VoyageDTO> fetchAllByUserUUID(@PathVariable(value = "id") String userUUID);
}
