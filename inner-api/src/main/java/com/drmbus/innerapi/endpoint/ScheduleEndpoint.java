package com.drmbus.innerapi.endpoint;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/15/2020
 */

import com.drmbus.innerapi.dto.ScheduleDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

@OpenAPIDefinition(
        info = @Info(
                title = "Schedule",
                version = "1.0",
                description = "Schedule inner API"
        )
)
@RequestMapping(value = "api/schedules", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ScheduleEndpoint {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    ScheduleDTO create(@RequestBody @Valid ScheduleDTO scheduleDTO);

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    ScheduleDTO fetch(@PathVariable(value = "id") Long scheduleId);

    @PutMapping
    @ResponseStatus(code = HttpStatus.OK)
    ScheduleDTO update(@RequestBody @Valid ScheduleDTO scheduleDTO);

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void delete(@PathVariable(value = "id") Long scheduleId);

}
