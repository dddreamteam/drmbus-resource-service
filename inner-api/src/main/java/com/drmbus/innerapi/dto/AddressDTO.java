package com.drmbus.innerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class    AddressDTO {

    private Long addressId;

    @NotBlank(message = "{validation.address.country}")
    private String country;

    @NotBlank(message = "{validation.address.zone}")
    private String zone;

    @NotBlank(message = "{validation.address.city}")
    private String city;

    @NotBlank(message = "{validation.address.street}")
    private String street;

    @NotBlank(message = "{validation.address.building}")
    private String building;

    @NotBlank(message = "{validation.address.office}")
    private String office;

    @NotBlank(message = "{validation.address.zip}")
    private String zip;

    public AddressDTO(String country, String zone, String city,
                      String street, String building, String office,
                      String zip) {
        this.country = country;
        this.zone = zone;
        this.city = city;
        this.street = street;
        this.building = building;
        this.office = office;
        this.zip = zip;
    }
}
