package com.drmbus.innerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InnerUserDTO {

    @NotBlank(message = "UUID cannot be null or whitespace")
    private UUID uuid; // unique user id

    // login
    @NotBlank(message = "username cannot be null or whitespace")
    @Email(message = "username not valid")
    private String username;

    private String firstName;
    private String lastName;
    private String middleName;
    private String avatarURL;

    private List<String> roles;

    @NotEmpty(message = "Companies cannot be null or Empty")
    private List<@Valid CompanyDTO> companies;

    @NotEmpty(message = "CompanyIdToCRoleTypeMap cannot be null or Empty")
    private Map<@Min(value = 1, message = "CompanyId should not be less than 1") Long, @NotBlank String> companyIdToCRoleTypeMap;
}
