package com.drmbus.innerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class InnerUserRegistrationDTO {
    @NotBlank(message = "{validation.password.blank}")
    @Min(value = 8, message = "{validation.password.length}")
    private String password;
    @Email(message = "{validation.email.nonvalid}")
    private String username;
}
