package com.drmbus.innerapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumberDTO {

    private Long id;

    @NotBlank(message = "{validation.phone.nonvalid}")
    private String phone;

    @JsonIgnore
    private Long companyDetailsId;

    public PhoneNumberDTO(String phone) {
        this.phone = phone;
    }

    public PhoneNumberDTO(String phone, Long companyDetailsId) {
        this.phone = phone;
        this.companyDetailsId = companyDetailsId;
    }
}
