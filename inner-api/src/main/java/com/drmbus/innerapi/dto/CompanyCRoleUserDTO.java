package com.drmbus.innerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCRoleUserDTO {
    private Long id; // non-useful yet
    private CompanyDTO company;
    private InnerUserDTO uuid;
    private String cRole;

    //For creating instance
    public CompanyCRoleUserDTO(CompanyDTO company, InnerUserDTO uuid, String cRole) {
        this.company = company;
        this.uuid = uuid;
        this.cRole = cRole;
    }
}
