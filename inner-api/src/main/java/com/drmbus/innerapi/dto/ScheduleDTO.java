package com.drmbus.innerapi.dto;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/7/2020
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDTO {

    private Long id;
    private DRMRouteDTO routeDTO;
    private String type;

    private Integer interval;

    private Boolean monday;
    private Boolean tuesday;
    private Boolean wednesday;
    private Boolean thursday;
    private Boolean friday;
    private Boolean saturday;
    private Boolean sunday;

    public ScheduleDTO(Long id, DRMRouteDTO routeDTO, String type, Integer interval) {
        this.id = id;
        this.routeDTO = routeDTO;
        this.type = type;
        this.interval = interval;
    }

    public ScheduleDTO(DRMRouteDTO routeDTO, String type, Integer interval) {
        this.routeDTO = routeDTO;
        this.type = type;
        this.interval = interval;
    }

    public ScheduleDTO(Long id, DRMRouteDTO routeDTO, String type,
                       Boolean monday, Boolean tuesday, Boolean wednesday, Boolean thursday,
                       Boolean friday, Boolean saturday, Boolean sunday) {
        this.id = id;
        this.routeDTO = routeDTO;
        this.type = type;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }

    public ScheduleDTO(DRMRouteDTO routeDTO, String type, Boolean monday,
                       Boolean tuesday, Boolean wednesday, Boolean thursday,
                       Boolean friday, Boolean saturday, Boolean sunday) {
        this.routeDTO = routeDTO;
        this.type = type;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }

}
