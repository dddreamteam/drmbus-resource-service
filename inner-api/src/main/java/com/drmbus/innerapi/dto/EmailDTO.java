package com.drmbus.innerapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDTO {

    private Long id;

    @Email(message = "{validation.email.nonvalid}")
    private String email;

    @JsonIgnore
    private Long companyDetailsId;


    public EmailDTO(String email,
                    Long companyDetailsId) {
        this.email = email;
        this.companyDetailsId = companyDetailsId;
    }

    public EmailDTO(String email) {
        this.email = email;
    }
}
