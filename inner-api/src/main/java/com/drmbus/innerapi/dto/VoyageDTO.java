package com.drmbus.innerapi.dto;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoyageDTO {

    private Long id;

    @NotBlank(message = "{validation.voyage.status}")
    private String status;

    private DRMRouteDTO route;

    private ZonedDateTime departureDateTime;

    private List<SeatDTO> seats;

    public VoyageDTO(String status, DRMRouteDTO route, ZonedDateTime departureDateTime) {
        this.status = status;
        this.route = route;
        this.departureDateTime = departureDateTime;
    }
}
