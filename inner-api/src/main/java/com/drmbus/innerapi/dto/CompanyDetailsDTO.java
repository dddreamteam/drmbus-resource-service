package com.drmbus.innerapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDetailsDTO {
    private Long companyDetailsId;
    private Long companyId;
    @NotNull
    private List<EmailDTO> emails;
    @NotNull
    private List<PhoneNumberDTO> phoneNumbers;
// ! не валидировать
    private AddressDTO realAddress;

    public CompanyDetailsDTO(Long companyId,
                             List<EmailDTO> emails,
                             List<PhoneNumberDTO> phoneNumbers,
                             AddressDTO realAddress) {
        this.companyId = companyId;
        this.emails = emails;
        this.phoneNumbers = phoneNumbers;
        this.realAddress = realAddress;
    }
}