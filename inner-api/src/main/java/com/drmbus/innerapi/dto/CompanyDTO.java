package com.drmbus.innerapi.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO {

    private Long id;

    @NotBlank(message = "{validation.company.legalForm.empty}")
    private String legalForm;

    @NotBlank(message = "{validation.company.name.empty}")
    private String name;

  // @NotBlank(message = "{validation.company.ownerUser.empty}")
    private UUID ownerUser;

    @NotBlank(message = "{validation.company.edrpouInn.empty}")
    private String edrpo_inn;

    // For creating instance
    public CompanyDTO(UUID ownerUser, String legalForm, String name, String edrpo_inn) {
        this.ownerUser = ownerUser;
        this.legalForm = legalForm;
        this.name = name;
        this.edrpo_inn = edrpo_inn;
    }

}
