package com.drmbus.innerapi.dto;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/7/2020
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DRMRouteDTO {

    private Long id;
    private String number;
    private Long companyId;
    private Boolean isActive;
    private ZonedDateTime firstVoyageDate;
    private Byte deepSales;
    private Integer seatCount;
    private List<StationDTO> stations;

    public DRMRouteDTO(
            String number,
            Long companyId,
            Boolean isActive,
            ZonedDateTime firstVoyageDate,
            Byte deepSales,
            Integer seatCount,
            List<StationDTO> stations
    ) {
        this.number = number;
        this.companyId = companyId;
        this.isActive = isActive;
        this.firstVoyageDate = firstVoyageDate;
        this.deepSales = deepSales;
        this.seatCount = seatCount;
        this.stations = stations;
    }
}
