package com.drmbus.innerapi.dto;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StationDTO {

    private Long id;

    @NotBlank(message = "{validation.station.address}")
    private AddressDTO address;

    public StationDTO(AddressDTO address) {
        this.address = address;
    }
}
