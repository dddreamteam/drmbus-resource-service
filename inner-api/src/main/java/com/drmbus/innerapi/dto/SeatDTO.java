package com.drmbus.innerapi.dto;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeatDTO {

    private Long id;
    private Long voyageId;
    private String status;

    public SeatDTO(String status, Long voyageId) {
        this.voyageId = voyageId;
        this.status = status;
    }
}
