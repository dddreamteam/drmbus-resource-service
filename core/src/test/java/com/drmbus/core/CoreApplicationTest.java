package com.drmbus.core;

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.entity.Voyage;
import com.drmbus.core.mapper.InnerUserMapper;
import com.drmbus.core.repository.AddressRepository;
import com.drmbus.core.repository.CRoleRepository;
import com.drmbus.core.repository.CompanyCRoleUserRepository;
import com.drmbus.core.repository.CompanyDetailsRepository;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.core.repository.DRMRouteRepository;
import com.drmbus.core.repository.EmailRepository;
import com.drmbus.core.repository.UserRepository;
import com.drmbus.core.repository.VoyageRepository;
import com.drmbus.core.utile.VoyageStatus;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.util.Optional;


@SpringBootTest
class CoreApplicationTest {

    Logger logger = LoggerFactory.getLogger(CoreApplicationTest.class);


    UserRepository userRepository;

    CompanyRepository companyRepository;

    CRoleRepository cRoleRepository;

    CompanyCRoleUserRepository companyCRoleUserRepository;

    AddressRepository addressRepository;

    EmailRepository emailRepository;

    CompanyDetailsRepository companyDetailsRepository;

    InnerUserMapper innerUserMapper;

    VoyageRepository voyageRepository;

    @Autowired
    DRMRouteRepository drmRouteRepository;

    @Autowired
    public CoreApplicationTest(UserRepository userRepository,
                               CompanyRepository companyRepository,
                               CRoleRepository cRoleRepository,
                               CompanyCRoleUserRepository companyCRoleUserRepository,
                               AddressRepository addressRepository,
                               EmailRepository emailRepository,
                               CompanyDetailsRepository companyDetailsRepository,
                               InnerUserMapper innerUserMapper,
                               VoyageRepository voyageRepository
    ) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.cRoleRepository = cRoleRepository;
        this.companyCRoleUserRepository = companyCRoleUserRepository;
        this.addressRepository = addressRepository;
        this.emailRepository = emailRepository;
        this.companyDetailsRepository = companyDetailsRepository;
        this.innerUserMapper = innerUserMapper;
        this.voyageRepository = voyageRepository;
    }

    @Test
    void mainTest() {
        /*Optional<Company> byId = companyRepository.findById(1L);
        DRMRoute drmRoute = new DRMRoute(
                "123",
                byId.get(),
                true,
                ZonedDateTime.now(),
                (byte) 30
        );
        DRMRoute save = drmRouteRepository.save(drmRoute);
        Voyage voyage = new Voyage(
                save,
                VoyageStatus.WEEK_TO,
                ZonedDateTime.now()
        );
        voyageRepository.save(voyage);*/

        //User owner = new User("owner@email.com", "password");
        //User moder = new User("moder@email.com", "password");
        //User agent = new User("agent@email.com", "password");
//
//
        //CRole cRoleOwner = new CRole(CRoleType.C_ROLE_OWNER);
        //CRole cRoleModer = new CRole(CRoleType.C_ROLE_MANAGER);
        //CRole cRoleAgent = new CRole(CRoleType.C_ROLE_AGENT);
//
//
//
        //Company company = new Company(LegalForm.FOP, "NAME", owner);
//
//
//
        //CompanyCRoleUser companyCRoleUserOwner = new CompanyCRoleUser(company, owner, cRoleOwner);
//
//
        //CompanyCRoleUser companyCRoleUserModer = new CompanyCRoleUser(company, moder, cRoleModer);
//
//
//
        //CompanyCRoleUser companyCRoleUserAgent = new CompanyCRoleUser(company, agent, cRoleAgent);
//
//
//
        //Address address = new Address("Ukraine", "ZoneID", "Kyiv", "Street", "3", "234", "123234");
//
//
//
        //CompanyDetails companyDetails = new CompanyDetails(company, address);
//
//
        //Email email = new Email("email@mail.com", companyDetails);

//        cRoleRepository.save(cRoleOwner);
//        cRoleRepository.save(cRoleModer);
//        cRoleRepository.save(cRoleAgent);
//
//        userRepository.save(owner);
//
//        companyRepository.save(company);
//
//        companyCRoleUserRepository.save(companyCRoleUserOwner);
//
//        userRepository.save(moder);
//        userRepository.save(agent);
//        companyCRoleUserRepository.save(companyCRoleUserModer);
//        companyCRoleUserRepository.save(companyCRoleUserAgent);
//        addressRepository.save(address);
//        companyDetailsRepository.save(companyDetails);
//
//
//        emailRepository.save(email);

//       Optional<CompanyDetails> companyDetailsOptional = companyDetailsRepository.findById(1L);
//
//       CompanyDetails companyDetail = companyDetailsOptional.get();
//
//       System.out.println(companyDetail);
//

        //serDTO user = new UserDTO();
        //ser.setUuid(UUID.fromString("e27f1bb8-fbd9-4c9f-912e-45e8745405ed"));

        //alidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //alidator validator = factory.getValidator();

        //et<ConstraintViolation<UserDTO>> violations = validator.validate(user);

        //or (ConstraintViolation<UserDTO> violation : violations) {
        //   System.out.println("==================================================");
        //   logger.error(violation.getMessage());
        //   throw new ApiRequestException(violation.getMessage());
        //

        //ptional<User> user = userRepository.findByUuid(UUID.fromString("e27f1bb8-fbd9-4c9f-912e-45e8745405ed"));
        //InnerUserDTO userDTO = innerUserMapper.toDto(user.get());
        // logger.info(userDTO.toString());
    }
}