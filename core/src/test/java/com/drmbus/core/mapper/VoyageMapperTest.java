package com.drmbus.core.mapper;

import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.entity.Seat;
import com.drmbus.core.entity.Voyage;
import com.drmbus.core.repository.DRMRouteRepository;
import com.drmbus.core.repository.SeatRepository;
import com.drmbus.core.repository.VoyageRepository;
import com.drmbus.core.utile.SeatStatus;
import com.drmbus.core.utile.VoyageStatus;
import com.drmbus.innerapi.dto.SeatDTO;
import com.drmbus.innerapi.dto.VoyageDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VoyageMapperTest {

//    VoyageMapper voyageMapper;
//
//    @Autowired
//    public VoyageMapperTest(VoyageMapper voyageMapper) {
//        this.voyageMapper = voyageMapper;
//    }
//
//    @Test
//    void toVoyageDTO() {
//        Voyage voyage = new Voyage(
//                new DRMRoute(),
//                VoyageStatus.WEEK_TO,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00")
//        );
//        VoyageDTO voyageDTO = new VoyageDTO(
//                "WEEK_TO",
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00")
//        );
//
//        VoyageDTO convertedVoyageDTO = voyageMapper.toVoyageDTO(voyage);
//
//        assertEquals(voyageDTO, convertedVoyageDTO);
//    }
//
//    @Test
//    void toVoyage() {
//
//        Voyage voyage = new Voyage(
//                new DRMRoute(),
//                VoyageStatus.WEEK_TO,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00")
//        );
//        VoyageDTO voyageDTO = new VoyageDTO(
//                "WEEK_TO",
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00")
//        );
//
//        Voyage convertedVoyage = voyageMapper.toVoyage(voyageDTO);
//
//        assertEquals(voyage.getId(), convertedVoyage.getId());
//        assertEquals(voyage.getStatus(), convertedVoyage.getStatus());
//        assertEquals(voyage.getDepartureDateTime(), convertedVoyage.getDepartureDateTime());
//    }
}