package com.drmbus.core.mapper;

import com.drmbus.core.entity.Seat;
import com.drmbus.core.entity.Voyage;
import com.drmbus.core.repository.SeatRepository;
import com.drmbus.core.repository.VoyageRepository;
import com.drmbus.core.utile.SeatStatus;
import com.drmbus.innerapi.dto.SeatDTO;
import com.drmbus.innerapi.dto.VoyageDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SeatMapperTest {

//    SeatMapper seatMapper;
//    VoyageRepository voyageRepository;
//    VoyageMapper voyageMapper;
//    SeatRepository seatRepository;
//
//    @Autowired
//    public SeatMapperTest(
//            SeatMapper seatMapper,
//            VoyageRepository voyageRepository,
//            VoyageMapper voyageMapper,
//            SeatRepository seatRepository
//    ) {
//        this.seatMapper = seatMapper;
//        this.voyageRepository = voyageRepository;
//        this.voyageMapper = voyageMapper;
//        this.seatRepository = seatRepository;
//    }
/*
    @Test
    void toSeatDTO() {
        Optional<Voyage> voyageById = voyageRepository.findById(1L);
        assertTrue(voyageById.isPresent());
        Voyage voyage = voyageById.get();

        VoyageDTO voyageDTO = voyageMapper.toVoyageDTO(voyage);
        SeatDTO seatDTO = new SeatDTO("AVAILABLE", voyageDTO);
        Seat seat = new Seat(SeatStatus.AVAILABLE, voyage);

        SeatDTO convertedSeatDTO = seatMapper.toSeatDTO(seat);
        assertEquals(seatDTO, convertedSeatDTO);
    }

    @Test
    void toSeat() {
        Optional<Voyage> voyageById = voyageRepository.findById(1L);
        assertTrue(voyageById.isPresent());
        Voyage voyage = voyageById.get();
        VoyageDTO voyageDTO = voyageMapper.toVoyageDTO(voyage);
        SeatDTO seatDTO = new SeatDTO("AVAILABLE", voyageDTO);
        Seat seat = new Seat(SeatStatus.AVAILABLE, voyage);

        Seat convertedSeat = seatMapper.toSeat(seatDTO);
        assertEquals(seat.getStatus(), convertedSeat.getStatus());
        assertEquals(seat.getVoyage().getId(), convertedSeat.getVoyage().getId());
    }*/

   /* @Test
    void seatRepository() {
        Optional<Voyage> voyageById = voyageRepository.findById(1L);
        assertTrue(voyageById.isPresent());
        Voyage voyage = voyageById.get();
        for (int i = 0; i < 40; i++) {
            Seat seat = new Seat(
                    SeatStatus.AVAILABLE,
                    voyage
            );
            seat = seatRepository.saveAndFlush(seat);
            assertNotNull(seat.getId());
            assertEquals(seat.getVoyage().getId(), voyage.getId());
        }
    }*/
}