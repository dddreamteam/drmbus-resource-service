package com.drmbus.core.mapper;

import com.drmbus.core.entity.Address;
import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.entity.Schedule;
import com.drmbus.core.entity.Station;
import com.drmbus.core.entity.User;
import com.drmbus.core.utile.LegalForm;
import com.drmbus.core.utile.ScheduleType;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.dto.ScheduleDTO;
import com.drmbus.innerapi.dto.StationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ScheduleMapperTest {

//    ScheduleMapper scheduleMapper;
//    StationMapper stationMapper;
//
//    @Autowired
//    public ScheduleMapperTest(
//            ScheduleMapper scheduleMapper,
//            StationMapper stationMapper
//    ) {
//        this.scheduleMapper = scheduleMapper;
//        this.stationMapper = stationMapper;
//    }
//
//    @Test
//    void toScheduleDTO() {
//        Company company = new Company(
//                LegalForm.FOP,
//                "Vasya",
//                new User()
//        );
//        company.setId(1L);
//        Address address = new Address(
//                "Ukaraine",
//                "UA",
//                "Kyiv",
//                "Joanna Pavla",
//                "32",
//                "13",
//                "1331"
//        );
//        Station station = new Station(address);
//        List<Station> stations = new ArrayList<>();
//        List<StationDTO> stationsDTOs = new ArrayList<>();
//        for (int i = 0; i < 7; i++) {
//            stations.add(station);
//            stationsDTOs.add(stationMapper.toStationDTO(station));
//        }
//        DRMRoute drmRoute = new DRMRoute(
//                "358",
//                company,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13
//        );
//        drmRoute.setStations(stations);
//        DRMRouteDTO drmRouteDTO = new DRMRouteDTO(
//                "358",
//                1L,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13,
//                stationsDTOs
//        );
//
//        int interval = 45;
//        Schedule schedule = new Schedule(
//                1L,
//                drmRoute,
//                ScheduleType.INTERVAL,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true
//        );
//        schedule.setInterval(interval);
//        ScheduleDTO scheduleDTO = new ScheduleDTO(
//                1L,
//                drmRouteDTO,
//                "INTERVAL",
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true
//        );
//        scheduleDTO.setInterval(interval);
//
//        ScheduleDTO CscheduleDTO = scheduleMapper.toScheduleDTO(schedule);
//
//        assertEquals(scheduleDTO, CscheduleDTO);
//    }
//
//    @Test
//    void toSchedule() {
//        Company company = new Company(
//                LegalForm.FOP,
//                "Vasya",
//                new User()
//        );
//        company.setId(1L);
//        Address address = new Address(
//                "Ukaraine",
//                "UA",
//                "Kyiv",
//                "Joanna Pavla",
//                "32",
//                "13",
//                "1331"
//        );
//        Station station = new Station(address);
//        List<Station> stations = new ArrayList<>();
//        List<StationDTO> stationsDTOs = new ArrayList<>();
//        for (int i = 0; i < 7; i++) {
//            stations.add(station);
//            stationsDTOs.add(stationMapper.toStationDTO(station));
//        }
//        DRMRoute drmRoute = new DRMRoute(
//                "358",
//                company,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13
//        );
//        drmRoute.setStations(stations);
//        DRMRouteDTO drmRouteDTO = new DRMRouteDTO(
//                "358",
//                1L,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13,
//                stationsDTOs
//        );
//
//        int interval = 45;
//        Schedule schedule = new Schedule(
//                1L,
//                drmRoute,
//                ScheduleType.INTERVAL,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true
//        );
//        schedule.setInterval(interval);
//        ScheduleDTO scheduleDTO = new ScheduleDTO(
//                1L,
//                drmRouteDTO,
//                "INTERVAL",
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true
//        );
//        scheduleDTO.setInterval(interval);
//
//        Schedule Cschedule = scheduleMapper.toSchedule(scheduleDTO);
//
//        assertEquals(schedule.getId(), Cschedule.getId());
//        assertEquals(schedule.getType(), Cschedule.getType());
//        assertEquals(schedule.getInterval(), Cschedule.getInterval());
//        assertEquals(schedule.getSunday(), Cschedule.getSunday());
//        assertEquals(schedule.getMonday(), Cschedule.getMonday());
//        assertEquals(schedule.getTuesday(), Cschedule.getTuesday());
//        assertEquals(schedule.getWednesday(), Cschedule.getWednesday());
//        assertEquals(schedule.getThursday(), Cschedule.getThursday());
//        assertEquals(schedule.getFriday(), Cschedule.getFriday());
//        assertEquals(schedule.getSaturday(), Cschedule.getSaturday());
//        assertEquals(schedule.getRoute().getId(), Cschedule.getRoute().getId());
//        assertEquals(schedule.getRoute().getCompany().getId(), Cschedule.getRoute().getCompany().getId());
//    }
}