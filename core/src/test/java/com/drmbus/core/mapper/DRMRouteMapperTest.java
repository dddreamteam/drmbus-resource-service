package com.drmbus.core.mapper;

import com.drmbus.core.entity.Address;
import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.entity.Station;
import com.drmbus.core.entity.User;
import com.drmbus.core.utile.LegalForm;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.dto.StationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DRMRouteMapperTest {

//    DRMRouteMapper drmRouteMapper;
//    StationMapper stationMapper;
//
//    @Autowired
//    public DRMRouteMapperTest(
//            DRMRouteMapper drmRouteMapper,
//            StationMapper stationMapper
//    ) {
//        this.drmRouteMapper = drmRouteMapper;
//        this.stationMapper = stationMapper;
//    }
//
//    @Test
//    void toDRMRouteDTO() {
//        Company company = new Company(
//                LegalForm.FOP,
//                "Vasya",
//                new User()
//        );
//        company.setId(1L);
//        Address address = new Address(
//                "Ukaraine",
//                "UA",
//                "Kyiv",
//                "Joanna Pavla",
//                "32",
//                "13",
//                "1331"
//        );
//        Station station = new Station(address);
//        List<Station> stations = new ArrayList<>();
//        List<StationDTO> stationsDTOs = new ArrayList<>();
//        for (int i = 0; i < 7; i++) {
//            stations.add(station);
//            stationsDTOs.add(stationMapper.toStationDTO(station));
//        }
//
//        DRMRoute drmRoute = new DRMRoute(
//                "358",
//                company,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13
//        );
//        drmRoute.setStations(stations);
//        DRMRouteDTO drmRouteDTO = new DRMRouteDTO(
//                "358",
//                1L,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13,
//                stationsDTOs
//        );
//
//        DRMRouteDTO CdrmRouteDTO = drmRouteMapper.toDRMRouteDTO(drmRoute);
//
//        assertEquals(drmRouteDTO, CdrmRouteDTO);
//    }
//
//    @Test
//    void toDRMRoute() {
//        Company company = new Company(
//                LegalForm.FOP,
//                "Vasya",
//                new User()
//        );
//        company.setId(1L);
//        Address address = new Address(
//                "Ukaraine",
//                "UA",
//                "Kyiv",
//                "Joanna Pavla",
//                "32",
//                "13",
//                "1331"
//        );
//        Station station = new Station(address);
//        List<Station> stations = new ArrayList<>();
//        List<StationDTO> stationsDTOs = new ArrayList<>();
//        for (int i = 0; i < 7; i++) {
//            stations.add(station);
//            stationsDTOs.add(stationMapper.toStationDTO(station));
//        }
//
//        DRMRoute drmRoute = new DRMRoute(
//                "358",
//                company,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13
//        );
//        drmRoute.setStations(stations);
//        DRMRouteDTO drmRouteDTO = new DRMRouteDTO(
//                "358",
//                1L,
//                true,
//                ZonedDateTime.parse("2011-12-03T10:15:30+01:00"),
//                (byte) 13,
//                stationsDTOs
//        );
//
//        DRMRoute CdrmRoute = drmRouteMapper.toDRMRoute(drmRouteDTO);
//
//        assertEquals(drmRoute.getNumber(), CdrmRoute.getNumber());
//        assertEquals(drmRoute.getCompany().getId(), CdrmRoute.getCompany().getId());
//        assertEquals(drmRoute.getIsActive(), CdrmRoute.getIsActive());
//        assertEquals(drmRoute.getFirstVoyageDate(), CdrmRoute.getFirstVoyageDate());
//        assertEquals(drmRoute.getDeepSales(), drmRoute.getDeepSales());
//    }
}