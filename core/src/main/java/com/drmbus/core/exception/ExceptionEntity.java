package com.drmbus.core.exception;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

/**
 * Created by Sevriukov M.K.
 * Date: 10.01.2020
 * Time: 10:03
 */


public class ExceptionEntity {
    private String message;
    private HttpStatus httpStatus;
    private ZonedDateTime timestamp;

    public ExceptionEntity(String message, HttpStatus httpStatus, ZonedDateTime timestamp) {
        this.message = message;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }
}
