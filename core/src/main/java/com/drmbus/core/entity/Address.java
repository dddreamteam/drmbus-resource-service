package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZoneId;
import java.util.StringJoiner;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long addressId;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "zone_id")
    private String zone; // TODO assign google API

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "building", nullable = false)
    private String building;

    @Column(name= "office", nullable = false)
    private String office;

    @Column(name = "zip", nullable = false)
    private String zip;

    @OneToOne(mappedBy = "legalAddress")
    private PayDetails payDetails;

    @OneToOne(mappedBy = "realAddress")
    private CompanyDetails companyDetails;

    @OneToOne(mappedBy = "address")
    private Station station;

    public Address(String country,
                   String zone,
                   String city,
                   String street,
                   String building,
                   String office,
                   String zip) {
        this.country = country;
        this.zone = zone;
        this.city = city;
        this.street = street;
        this.building = building;
        this.office = office;
        this.zip = zip;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Address.class.getSimpleName() + "[", "]")
                .add("addressId=" + addressId)
                .add("country='" + country + "'")
                .add("zone='" + zone + "'")
                .add("city='" + city + "'")
                .add("street='" + street + "'")
                .add("building='" + building + "'")
                .add("office='" + office + "'")
                .add("zip='" + zip + "'")
                .toString();
    }
}
