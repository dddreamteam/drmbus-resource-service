package com.drmbus.core.entity;


import com.drmbus.core.utile.LegalForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;


@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "legal_form", nullable = false)
    @Enumerated(EnumType.STRING)
    private LegalForm legalForm;

    @Column(name = "name", nullable = false)
    private String name;

    private String edrpo_inn;

    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @ManyToOne
    @JoinColumn(name = "owner_uuid", nullable = false)
    private User ownerUser;

    @OneToOne(mappedBy = "company", cascade = CascadeType.ALL)
    private CompanyDetails companyDetails;

    @OneToOne(mappedBy = "company")
    private  PayDetails payDetails;

    @OneToMany(mappedBy = "company")
    private List<DRMRoute> routes;

    @OneToMany(mappedBy = "company")
    private List<CompanyCRoleUser> members;

    public Company(LegalForm legalForm, String name, User ownerUser) {
        this.legalForm = legalForm;
        this.name = name;
        this.ownerUser = ownerUser;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Company.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("legalForm=" + legalForm)
                .add("name='" + name + "'")
                .add("version=" + version)
                .add("lastModifiedDate=" + lastModifiedDate)
                .add("ownerUser=" + ownerUser)
                .toString();
    }
}
