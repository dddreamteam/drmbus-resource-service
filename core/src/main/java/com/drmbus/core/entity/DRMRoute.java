package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "drm_route")
public class DRMRoute {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    private String number;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "first_voyage_date",  columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime firstVoyageDate;

    @Column(name = "deep_sales")
    private Byte deepSales;

    @Column(name = "seat_count")
    private Integer seatCount;

    @ManyToMany
    @JoinTable(
            name = "routes_stations",
            joinColumns = @JoinColumn(name = "route_id"),
            inverseJoinColumns = @JoinColumn(name = "station_id")
    )
    @OrderColumn(name = "ordered_station")
    private List<Station> stations;

    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY)
    private List<Voyage> voyages;

    /*Пока без стоимости между станциями*/

    @OneToOne(mappedBy = "route", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Schedule schedule;

    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    public DRMRoute(String number,
                    Company company,
                    Boolean isActive,
                    ZonedDateTime firstVoyageDate,
                    Integer seatCount,
                    Byte deepSales) {
        this.number = number;
        this.company = company;
        this.isActive = isActive;
        this.firstVoyageDate = firstVoyageDate;
        this.deepSales = deepSales;
        this.seatCount = seatCount;
    }
}
