package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class CRole {


    @Id
    private Long id;

    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "cRole")
    private List<CompanyCRoleUser> companyCRoleUsers;

    public CRole(Long id, String type) {
        this.id = id;
        this.type = type;
    }



    @Override
    public String toString() {
        return new StringJoiner(", ", CRole.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("role=" + type)
                .toString();
    }
}
