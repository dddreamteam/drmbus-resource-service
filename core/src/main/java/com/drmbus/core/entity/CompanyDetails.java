package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.LastModifiedDate;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "company_details")
public class CompanyDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long companyDetailsId;

    @OneToOne
    @JoinColumn(name = "company_id", nullable = false, unique = true)
    private Company company;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "details")
    private List<Email> emails;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "details")
    private List<PhoneNumber> phoneNumbers;

    @OneToOne
    @JoinColumn(name = "address_real_id")
    private Address realAddress;

    /*********************/
    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @Override
    public String toString() {
        return new StringJoiner(", ", CompanyDetails.class.getSimpleName() + "[", "]")
                .add("companyDetailsId=" + companyDetailsId)
                .add("email=" + emails)
                .add("phone=" + phoneNumbers)
                .add("realAddress=" + realAddress)
                .add("version=" + version)
                .add("lastModifiedDate=" + lastModifiedDate)
                .toString();
    }
}
