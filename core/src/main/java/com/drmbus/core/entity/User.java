package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "drm_user")
public class User {

    @Id
    @Column(name = "uuid", unique = true, nullable = false)
    private UUID uuid; // unique user id

    @NotBlank(message = "Password cannot be null or whitespace")
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "username", unique = true, nullable = false) // login
    private String username;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "account_non_expired", nullable = false)
    private boolean accountNonExpired;

    @Column(name = "credentials_non_expired", nullable = false)
    private boolean credentialsNonExpired;

    @Column(name = "account_non_locked", nullable = false)
    private boolean accountNonLocked;

    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "grole_user",
            joinColumns = {@JoinColumn(name = "user_uuid", referencedColumnName = "uuid")},
            inverseJoinColumns = {@JoinColumn(name = "grole_id", referencedColumnName = "grole_id")})
    private List<GRole> gRoles;

    //  User details
    @OneToOne(mappedBy = "user")
    private UserDetails details;

    //  Владелец / Owner
    @OneToMany(mappedBy = "ownerUser")
    private List<Company> companies;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "user")
    private List<CompanyCRoleUser> companyCRoleUsers;

    public User(UUID uuid,
                String password,
                String username,
                boolean enabled,
                boolean accountNonExpired,
                boolean credentialsNonExpired,
                boolean accountNonLocked,
                List<GRole> gRoles) {
        this.uuid = uuid;
        this.password = password;
        this.username = username;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.gRoles = gRoles;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("uuid=" + uuid)
                .add("password='" + password + "'")
                .add("username='" + username + "'")
                .add("enabled=" + enabled)
                .add("accountNonExpired=" + accountNonExpired)
                .add("credentialsNonExpired=" + credentialsNonExpired)
                .add("accountNonLocked=" + accountNonLocked)
                .toString();
    }
}
