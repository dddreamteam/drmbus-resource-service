package com.drmbus.core.entity;

import com.drmbus.core.utile.VoyageStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "voyage")
public class Voyage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "route_id", nullable = false)
    private DRMRoute route;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private VoyageStatus status;

    @Column(name = "departure_date_time",  columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime departureDateTime;

    @OneToMany(mappedBy = "voyage")
    private List<Seat> seats;

    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    public Voyage(DRMRoute route,
                  VoyageStatus status,
                  ZonedDateTime departureDateTime) {
        this.route = route;
        this.status = status;
        this.departureDateTime = departureDateTime;
    }
}
