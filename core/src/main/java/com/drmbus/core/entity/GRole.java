package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Global role
 * */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "grole")
public class GRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "grole_id")
    private Long roleId;

    @Column(name = "type")
    private String type;

    @ManyToMany(mappedBy = "gRoles")
    private List<User> users;

}
