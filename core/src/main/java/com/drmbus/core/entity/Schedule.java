package com.drmbus.core.entity;

import com.drmbus.core.utile.ScheduleType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "route_id", nullable = false)
    private DRMRoute route;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ScheduleType type;

    @Column(name = "interval")
    private Integer interval;

    @Column(name = "monday")
    private Boolean monday;
    @Column(name = "tuesday")
    private Boolean tuesday;
    @Column(name = "wednesday")
    private Boolean wednesday;
    @Column(name = "thursday")
    private Boolean thursday;
    @Column(name = "friday")
    private Boolean friday;
    @Column(name = "saturday")
    private Boolean saturday;
    @Column(name = "sunday")
    private Boolean sunday;

    public Schedule(Long id, DRMRoute route, ScheduleType type, Integer interval) {
        this.id = id;
        this.route = route;
        this.type = type;
        this.interval = interval;
    }

    public Schedule(DRMRoute route, ScheduleType type, Integer interval) {
        this.route = route;
        this.type = type;
        this.interval = interval;
    }

    public Schedule(Long id, DRMRoute route, ScheduleType type,
                    Boolean monday, Boolean tuesday, Boolean wednesday, Boolean thursday,
                    Boolean friday, Boolean saturday, Boolean sunday) {
        this.id = id;
        this.route = route;
        this.type = type;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }

    public Schedule(DRMRoute route, ScheduleType type, Boolean monday,
                    Boolean tuesday, Boolean wednesday, Boolean thursday,
                    Boolean friday, Boolean saturday, Boolean sunday) {
        this.route = route;
        this.type = type;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }
}
