package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.StringJoiner;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "phone_number")
public class PhoneNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "phone")
    private String phone;

    //  company details
    @ManyToOne
    @JoinTable(
            name = "company_details_phone",
            joinColumns = @JoinColumn(name = "phone_id"),
            inverseJoinColumns = @JoinColumn(name = "company_details_id")
    )
    private CompanyDetails details;

    public PhoneNumber(Long id, String phone) {
        this.id = id;
        this.phone = phone;
    }
    public PhoneNumber(String phone) {
        this.phone = phone;
    }
    @Override
    public String toString() {
        return new StringJoiner(", ", PhoneNumber.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("phone='" + phone + "'")
                .toString();
    }
}
