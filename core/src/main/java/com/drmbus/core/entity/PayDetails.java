package com.drmbus.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pay_details")
public class PayDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long payDetailsId;

    private String iban;


    @OneToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToOne
    @JoinColumn(name = "address_legal_id", nullable = false)
    private Address legalAddress;



    /*********************/
    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;
}
