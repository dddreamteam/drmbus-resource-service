package com.drmbus.core.utile;

public enum SeatStatus {
    AVAILABLE,
    SOLD,
    RESERVE,
    UNAVAILABLE
}
