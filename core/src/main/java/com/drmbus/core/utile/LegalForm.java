package com.drmbus.core.utile;

/**
 * LegalForm ENUM used for determinate {@link com.drmbus.core.entity.Company}
 * FOP
 * TOV
 */
public enum LegalForm {
    FOP,
    TOV,
}
