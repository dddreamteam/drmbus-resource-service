package com.drmbus.core.utile;

public enum VoyageStatus {
    WEEK_MORE,
    WEEK_TO,
    DAY_TO,
    ON_THE_WAY,
    COMPLETE,
    CANCELED
}
