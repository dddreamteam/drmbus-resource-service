package com.drmbus.core.endpoint;
import com.drmbus.core.service.CompanyDetailsService;
import com.drmbus.innerapi.dto.CompanyDetailsDTO;
import com.drmbus.innerapi.endpoint.CompanyDetailsEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CompanyDetailsEndpointImpl implements CompanyDetailsEndpoint {

    private final CompanyDetailsService service;

    @Autowired
    public CompanyDetailsEndpointImpl(CompanyDetailsService service) {
        this.service = service;
    }

    @Override
    public CompanyDetailsDTO create(@Valid CompanyDetailsDTO companyDetailsDTO) {
        return service.create(companyDetailsDTO);
    }

    @Override
    public CompanyDetailsDTO fetchByCompanyId(Long companyId) {
        return service.fetchByCompanyId(companyId);
    }

    @Override
    public CompanyDetailsDTO update(@Valid CompanyDetailsDTO companyDetailsDTO) {
        return service.update(companyDetailsDTO);
    }
}
