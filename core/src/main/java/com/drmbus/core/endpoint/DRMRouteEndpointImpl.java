package com.drmbus.core.endpoint;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/8/2020
 */


import com.drmbus.core.service.DRMRouteService;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.endpoint.DRMRouteEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class DRMRouteEndpointImpl implements DRMRouteEndpoint {

    private final DRMRouteService routeService;

    @Autowired
    public DRMRouteEndpointImpl(DRMRouteService routeService) {
        this.routeService = routeService;
    }

    @Override
    public DRMRouteDTO create(@Valid DRMRouteDTO routeDTO) {
        return routeService.create(routeDTO);
    }

    @Override
    public DRMRouteDTO fetch(Long routeId) {
        return routeService.fetch(routeId);
    }

    @Override
    public DRMRouteDTO update(@Valid DRMRouteDTO routeDTO) {
        return routeService.update(routeDTO);
    }

    @Override
    public void delete(Long routeId) {
        routeService.delete(routeId);
    }

    @Override
    public List<DRMRouteDTO> fetchAllRoutesByUUID(String userUUID) {
        return routeService.fetchAllByUserId(UUID.fromString(userUUID));
    }
}
