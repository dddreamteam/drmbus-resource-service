package com.drmbus.core.endpoint;

import com.drmbus.core.service.serviceImpl.CompanyServiceImpl;
import com.drmbus.core.service.serviceImpl.DRMRouteServiceImpl;
import com.drmbus.innerapi.dto.CompanyDTO;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.endpoint.CompanyEndpoint;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CompanyEndpointImpl implements CompanyEndpoint {

    private final DRMRouteServiceImpl drmRouteService;
    private final CompanyServiceImpl companyService;

    public CompanyEndpointImpl(DRMRouteServiceImpl drmRouteService, CompanyServiceImpl companyService) {
        this.drmRouteService = drmRouteService;
        this.companyService = companyService;
    }

    @Override
    public CompanyDTO create(@Valid CompanyDTO companyDTO) {
        return companyService.create(companyDTO);
    }

    @Override
    public CompanyDTO fetch(Long id) {
        return companyService.fetch(id);
    }

    @Override
    public CompanyDTO update(CompanyDTO companyDTO) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<CompanyDTO> fetchAll() {
        return null;
    }

    @Override
    public List<DRMRouteDTO> fetchAllByCompanyId(Long companyId) {
        return drmRouteService.fetchAllByCompanyId(companyId);
    }
}
