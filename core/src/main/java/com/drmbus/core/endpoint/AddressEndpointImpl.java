package com.drmbus.core.endpoint;


import com.drmbus.core.service.AddressService;
import com.drmbus.innerapi.dto.AddressDTO;
import com.drmbus.innerapi.endpoint.AddressEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AddressEndpointImpl  implements AddressEndpoint {

    private final AddressService service;

    @Autowired
    public AddressEndpointImpl(AddressService service) {
        this.service = service;
    }


    @Override
    public AddressDTO create(@Valid AddressDTO addressDTO) {
        return service.create(addressDTO);
    }

    @Override
    public AddressDTO fetchByCompanyId(Long companyId) {
        return service.fetch(companyId);
    }

    @Override
    public AddressDTO update(@Valid AddressDTO addressDTO) {
        return null;
    }
}
