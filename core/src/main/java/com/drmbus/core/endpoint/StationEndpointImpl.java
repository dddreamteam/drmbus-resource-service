package com.drmbus.core.endpoint;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/18/2020
 */

import com.drmbus.core.service.StationService;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import com.drmbus.innerapi.dto.StationDTO;
import com.drmbus.innerapi.endpoint.StationEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class StationEndpointImpl implements StationEndpoint {

    private final StationService stationService;

    @Autowired
    public StationEndpointImpl(StationService stationService) {
        this.stationService = stationService;
    }

    @Override
    public StationDTO create(@Valid StationDTO stationDTO) {
        return stationService.create(stationDTO);
    }

    @Override
    public StationDTO fetch(Long stationId) {
        return stationService.fetch(stationId);
    }

    @Override
    public StationDTO update(@Valid StationDTO stationDTO) {
        return stationService.update(stationDTO);
    }

    @Override
    public void delete(Long stationId) {
        stationService.delete(stationId);
    }

    @Override
    public List<StationDTO> fetchAll() {
        return stationService.fetchAll();
    }
}
