package com.drmbus.core.endpoint;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/15/2020
 */

import com.drmbus.core.service.ScheduleService;
import com.drmbus.innerapi.dto.ScheduleDTO;
import com.drmbus.innerapi.endpoint.ScheduleEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ScheduleEndpointImpl implements ScheduleEndpoint {

    private final ScheduleService scheduleService;

    @Autowired
    public ScheduleEndpointImpl(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @Override
    public ScheduleDTO create(@Valid ScheduleDTO scheduleDTO) {
        return scheduleService.create(scheduleDTO);
    }

    @Override
    public ScheduleDTO fetch(Long scheduleId) {
        return scheduleService.fetch(scheduleId);
    }

    @Override
    public ScheduleDTO update(@Valid ScheduleDTO scheduleDTO) {
        return scheduleService.update(scheduleDTO);
    }

    @Override
    public void delete(Long scheduleId) {
        scheduleService.delete(scheduleId);
    }
}
