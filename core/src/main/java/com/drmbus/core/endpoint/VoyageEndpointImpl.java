package com.drmbus.core.endpoint;

import com.drmbus.core.service.VoyageService;
import com.drmbus.innerapi.dto.VoyageDTO;
import com.drmbus.innerapi.endpoint.VoyageEndpoint;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class VoyageEndpointImpl implements VoyageEndpoint {

    private final VoyageService service;

    public VoyageEndpointImpl(VoyageService service) {
        this.service = service;
    }

    @Override
    public VoyageDTO create(@Valid VoyageDTO voyageDTO) {
        return service.create(voyageDTO);
    }

    @Override
    public VoyageDTO fetch(Long id) {
        return service.fetch(id);
    }

    @Override
    public VoyageDTO update(@Valid VoyageDTO voyageDTO) {
        return service.update(voyageDTO);
    }

    @Override
    public void delete(Long id) {
        service.delete(id);
    }

    @Override
    public List<VoyageDTO> fetchAllByUserUUID(String userUUID) {
        return service.findAllByUserId(UUID.fromString(userUUID));
    }
}
