package com.drmbus.core.endpoint;

import com.drmbus.core.service.UserService;
import com.drmbus.innerapi.dto.InnerUserDTO;
import com.drmbus.innerapi.dto.InnerUserRegistrationDTO;
import com.drmbus.innerapi.endpoint.UserEndpoint;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
public class UserEndpointImpl implements UserEndpoint {

    public UserService userService;

    public UserEndpointImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public InnerUserDTO registration(@Valid InnerUserRegistrationDTO user) {
        return userService.registration(user);
    }


    @Override
    public InnerUserDTO fetch(String id) {
        UUID fromString = UUID.fromString(id);
        return userService.fetchByUuid(fromString);
    }

    @Override
    public InnerUserDTO fetchByUsername(String username) {
        return userService.fetchByUsername(username);
    }

    //  @Override
    public InnerUserDTO update(InnerUserDTO userDTO) {
        return null;
    }

 //   @Override
    public void delete(Long id) {

    }
}
