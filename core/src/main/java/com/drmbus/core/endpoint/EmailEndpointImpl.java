package com.drmbus.core.endpoint;

import com.drmbus.core.service.EmailService;
import com.drmbus.innerapi.dto.EmailDTO;
import com.drmbus.innerapi.endpoint.EmailEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
public class EmailEndpointImpl implements EmailEndpoint {

    private final EmailService emailService;

    @Autowired
    public EmailEndpointImpl(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public EmailDTO create(@Valid EmailDTO emailDTO) {
        return emailService.create(emailDTO);
    }

    @Override
    public EmailDTO fetch(Long emailId) {
       return emailService.fetch(emailId);
    }

    @Override
    public EmailDTO update(@Valid EmailDTO emailDTO) {
        return emailService.update(emailDTO);
    }

    @Override
    public void delete(Long emailId) {
        emailService.delete(emailId);
    }
}
