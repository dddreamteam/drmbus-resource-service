package com.drmbus.core.service;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/3/2020
 */

import com.drmbus.innerapi.dto.EmailDTO;

public interface EmailService extends CFUDServiceInterface<EmailDTO, Long>  {
}
