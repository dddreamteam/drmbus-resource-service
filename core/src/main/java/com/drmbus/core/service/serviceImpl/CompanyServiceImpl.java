package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.CRole;
import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.CompanyCRoleUser;
import com.drmbus.core.mapper.CompanyMapper;
import com.drmbus.core.repository.CompanyCRoleUserRepository;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.core.service.CompanyService;
import com.drmbus.innerapi.dto.CompanyDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class CompanyServiceImpl implements CompanyService {

    private final CompanyMapper companyMapper;
    private final CompanyRepository companyRepository;
    private final CompanyCRoleUserRepository companyCRoleUserRepository;


    public CompanyServiceImpl(CompanyMapper companyMapper, CompanyRepository companyRepository, CompanyCRoleUserRepository companyCRoleUserRepository) {
        this.companyMapper = companyMapper;
        this.companyRepository = companyRepository;
        this.companyCRoleUserRepository = companyCRoleUserRepository;
    }

    @Override
    public CompanyDTO create(CompanyDTO companyDTO) {
        Company company = companyMapper.toCompany(companyDTO);
        Company companySaved = companyRepository.saveAndFlush(company);
        CompanyCRoleUser companyCRoleUser = new CompanyCRoleUser();
        companyCRoleUser.setCompany(companySaved);
        companyCRoleUser.setCRole(new CRole(1L, "C_ROLE_OWNER"));
        companyCRoleUser.setUser(companySaved.getOwnerUser());
        companyCRoleUserRepository.saveAndFlush(companyCRoleUser);
        return companyMapper.toCompanyDTO(companySaved);
    }

    @Override
    public CompanyDTO fetch(Long id) {
        Optional<Company> companyOptional = companyRepository.findById(id);
        if(companyOptional.isPresent())
        {
            return  companyMapper.toCompanyDTO(companyOptional.get());
        } else {
            throw new RuntimeException("Couldn't find Company in DB");
        }

    }

    @Override
    public CompanyDTO update(CompanyDTO companyDTO) {
        return null;
    }

    @Override
    public void delete(Long aLong) {

    }

}
