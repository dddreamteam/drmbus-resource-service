package com.drmbus.core.service;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/18/2020
 */

import com.drmbus.innerapi.dto.StationDTO;

import java.util.List;

public interface StationService extends CFUDServiceInterface<StationDTO, Long> {
    List<StationDTO> fetchAll();
}
