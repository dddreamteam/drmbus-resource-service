package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.Address;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.mapper.AddressMapper;
import com.drmbus.core.repository.AddressRepository;
import com.drmbus.core.service.AddressService;
import com.drmbus.innerapi.dto.AddressDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    private final AddressMapper mapper;
    private final AddressRepository repository;

    @Autowired
    public AddressServiceImpl(AddressMapper mapper, AddressRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    @Override
    public AddressDTO create(AddressDTO addressDTO) {
        Set<ConstraintViolation<AddressDTO>> validated = validator.validate(addressDTO);
        validated.forEach(action -> {
            log.error(action.getPropertyPath().toString() + " can't be null");
            throw new RequestException(action.getPropertyPath().toString() + " can't be null");
        });
        Address address = mapper.toAddress(addressDTO);
        return mapper.toAddressDTO(repository.saveAndFlush(address));
    }

    @Override
    public AddressDTO fetch(Long id) {
        Optional<Address> optional = repository.findById(id);
        Address address = optional.orElseGet(() -> {
            log.error("Address by id= " + id + " can't be found in DB");
            throw new RequestException("Address can't be found in DB");
        });
        return mapper.toAddressDTO(address);
    }

    @Override
    public AddressDTO update(AddressDTO addressDTO) {
        return create(addressDTO);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
