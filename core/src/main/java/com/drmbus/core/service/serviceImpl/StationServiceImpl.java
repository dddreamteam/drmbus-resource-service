package com.drmbus.core.service.serviceImpl;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/18/2020
 */

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.Station;
import com.drmbus.core.mapper.StationMapper;
import com.drmbus.core.repository.StationRepository;
import com.drmbus.core.service.StationService;
import com.drmbus.innerapi.dto.StationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StationServiceImpl implements StationService {

    private final StationRepository stationRepository;
    private final StationMapper stationMapper;

    public StationServiceImpl(
            StationRepository stationRepository,
            StationMapper stationMapper
    ) {
        this.stationRepository = stationRepository;
        this.stationMapper = stationMapper;
    }

    @Override
    public StationDTO create(StationDTO stationDTO) {
        Station station = stationMapper.toStation(stationDTO);
        Station stationFromDB = stationRepository.saveAndFlush(station);
        return stationMapper.toStationDTO(stationFromDB);
    }

    @Override
    public StationDTO fetch(Long stationId) {
        Optional<Station> stationOptional = stationRepository.findById(stationId);
        if (stationOptional.isPresent()) {
            return stationMapper.toStationDTO(stationOptional.get());
        } else {
            log.error("Couldn't find Station with ID = " + stationId + " in DB");
            throw new RuntimeException("Couldn't find Station with such ID in DB");
        }
    }

    @Override
    public StationDTO update(StationDTO stationDTO) {
        Station station = stationMapper.toStation(stationDTO);
        Station stationFromDB = stationRepository.saveAndFlush(station);
        return stationMapper.toStationDTO(stationFromDB);
    }

    @Override
    public void delete(Long stationId) {
        stationRepository.deleteById(stationId);
    }

    @Override
    public List<StationDTO> fetchAll() {
        List<Station> stations = stationRepository.findAll();
        return stations
                .stream()
                .map(stationMapper::toStationDTO)
                .collect(Collectors.toList());
    }
}
