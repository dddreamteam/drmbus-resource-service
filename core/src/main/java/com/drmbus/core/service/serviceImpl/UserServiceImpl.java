package com.drmbus.core.service.serviceImpl;


import com.drmbus.core.entity.GRole;
import com.drmbus.core.entity.User;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.mapper.InnerUserMapper;
import com.drmbus.core.repository.UserRepository;

import com.drmbus.core.service.GRoleService;
import com.drmbus.core.service.UserService;
import com.drmbus.innerapi.dto.InnerUserDTO;
import com.drmbus.innerapi.dto.InnerUserRegistrationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final InnerUserMapper innerUserMapper;
    private final GRoleService gRoleService;

    public UserServiceImpl(UserRepository userRepository, InnerUserMapper innerUserMapper, GRoleService gRoleService) {
        this.userRepository = userRepository;
        this.innerUserMapper = innerUserMapper;
        this.gRoleService = gRoleService;
    }

    @Override
    public InnerUserDTO registration(InnerUserRegistrationDTO innerUserRegistrationDTO) {
        Optional<User> userByEmail = userRepository.findByUsername(innerUserRegistrationDTO.getUsername());
        if (userByEmail.isEmpty()) {
            // GRole id 3 - G_ROLE_CLIENT
            GRole role = gRoleService.fetch(3L);
            List<GRole> gRoles = new ArrayList<>();
            gRoles.add(role);
            User user = new User(
                    UUID.randomUUID(),
                    innerUserRegistrationDTO.getPassword(),
                    innerUserRegistrationDTO.getUsername(),
                    true,
                    true,
                    true,
                    true,
                    gRoles
            );
            return innerUserMapper.toInnerUserDTO(userRepository.save(user));
        } else throw new RequestException("User with this email is already exist");
    }

    @Override
    public InnerUserDTO fetchByUuid(UUID uuid) {
        Optional<User> optionalUser = userRepository.findByUuid(uuid);

        if (optionalUser.isPresent()) return innerUserMapper.toInnerUserDTO(optionalUser.get());
        else {
            log.error("User with uuid = " + uuid + " not found in DB");
            throw new RequestException("User with uuid = " + uuid + " not found in DB"); // TODO
        }
    }

    @Override
    public InnerUserDTO fetchByUsername(String username) {
        Optional<User> userByEmail = userRepository.findByUsername(username);
        userByEmail.orElseThrow(() -> {
            throw new RequestException("User with uuid = " + username + " not found in DB");
        });
        return innerUserMapper.toInnerUserDTO(userByEmail.get());
    }
}
