package com.drmbus.core.service;

public interface CFUDServiceInterface<E, ID> {

    E create(E e);
    E fetch(ID id);
    E update(E e);
    void delete(ID id);

}
