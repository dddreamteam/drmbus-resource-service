package com.drmbus.core.service;

import com.drmbus.innerapi.dto.PhoneNumberDTO;

public interface PhoneNumberService extends CFUDServiceInterface<PhoneNumberDTO, Long> {
}
