package com.drmbus.core.service;

import com.drmbus.innerapi.dto.CompanyDTO;

public interface CompanyService extends CFUDServiceInterface<CompanyDTO, Long> {

}
