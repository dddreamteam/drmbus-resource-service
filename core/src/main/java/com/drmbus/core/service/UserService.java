package com.drmbus.core.service;

import com.drmbus.core.exception.RequestException;
import com.drmbus.innerapi.dto.InnerUserDTO;
import com.drmbus.innerapi.dto.InnerUserRegistrationDTO;

import java.util.UUID;

public interface UserService {

    /**
     * This method used for Carrier and Agent registration.
     * <p>
     * Returns an InnerUserDTO object that can then be used for authentication.
     * <p>
     *
     * @param  innerUserRegistrationDTO  an object {@link InnerUserRegistrationDTO} giving the fields
     *                                   that used for registration and authentication
     * @throws RequestException       with status 409 "Conflict" if email is already exist in database
     * @return {@link InnerUserDTO }
     * */
    InnerUserDTO registration(InnerUserRegistrationDTO innerUserRegistrationDTO);

    InnerUserDTO fetchByUuid(UUID uuid);

    InnerUserDTO fetchByUsername(String username);
}
