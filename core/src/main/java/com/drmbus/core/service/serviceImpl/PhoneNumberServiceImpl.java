package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.PhoneNumber;
import com.drmbus.core.mapper.PhoneNumberMapper;
import com.drmbus.core.repository.PhoneNumberRepository;
import com.drmbus.core.service.PhoneNumberService;
import com.drmbus.innerapi.dto.PhoneNumberDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private final PhoneNumberRepository phoneNumberRepository;
    private final PhoneNumberMapper mapper;

    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository,
                                  PhoneNumberMapper mapper) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.mapper = mapper;
    }

    @Override
    public PhoneNumberDTO create(PhoneNumberDTO phoneNumberDTO) {
        PhoneNumber phoneNumber = mapper.toPhoneNumber(phoneNumberDTO);
        PhoneNumber phoneNumberSaved = phoneNumberRepository.saveAndFlush(phoneNumber);
        return mapper.toPhoneNumberDTO(phoneNumberSaved);
    }

    @Override
    public PhoneNumberDTO fetch(Long aLong) {
        return null;
    }

    @Override
    public PhoneNumberDTO update(PhoneNumberDTO phoneNumberDTO) {
        return null;
    }

    @Override
    public void delete(Long aLong) {

    }
}
