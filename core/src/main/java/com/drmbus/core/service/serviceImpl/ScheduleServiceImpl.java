package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.Schedule;
import com.drmbus.core.mapper.ScheduleMapper;
import com.drmbus.core.repository.ScheduleRepository;
import com.drmbus.core.service.ScheduleService;
import com.drmbus.innerapi.dto.ScheduleDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/15/2020
 */

@Service
@Slf4j
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository scheduleRepository;
    private final ScheduleMapper scheduleMapper;

    public ScheduleServiceImpl(
            ScheduleRepository scheduleRepository,
            ScheduleMapper scheduleMapper
    ) {
        this.scheduleRepository = scheduleRepository;
        this.scheduleMapper = scheduleMapper;
    }

    @Override
    public ScheduleDTO create(ScheduleDTO scheduleDTO) {
        Schedule scheduleNoId = scheduleMapper.toSchedule(scheduleDTO);
        Schedule schedule = scheduleRepository.saveAndFlush(scheduleNoId);
        return scheduleMapper.toScheduleDTO(schedule);
    }

    @Override
    public ScheduleDTO fetch(Long scheduleId) {
        Optional<Schedule> scheduleById = scheduleRepository.findById(scheduleId);
        if (scheduleById.isPresent()) {
            return scheduleMapper.toScheduleDTO(scheduleById.get());
        } else {
            log.error("Couldn't find Schedule with ID = " + scheduleId + " in DB");
            throw new RuntimeException("Couldn't find Schedule with such ID in DB");
        }
    }

    @Override
    public ScheduleDTO update(ScheduleDTO scheduleDTO) {
        Schedule schedule = scheduleMapper.toSchedule(scheduleDTO);
        Schedule scheduleFromDB = scheduleRepository.saveAndFlush(schedule);
        return scheduleMapper.toScheduleDTO(scheduleFromDB);
    }

    @Override
    public void delete(Long scheduleId) {
        scheduleRepository.deleteById(scheduleId);
    }
}
