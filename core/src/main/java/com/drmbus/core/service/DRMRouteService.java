package com.drmbus.core.service;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/8/2020
 */

import com.drmbus.innerapi.dto.DRMRouteDTO;

import java.util.List;
import java.util.UUID;

public interface DRMRouteService extends CFUDServiceInterface<DRMRouteDTO, Long> {
    List<DRMRouteDTO> fetchAllByCompanyId(Long companyId);

    List<DRMRouteDTO> fetchAllByUserId(UUID userId);
}
