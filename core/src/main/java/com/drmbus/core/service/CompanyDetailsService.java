package com.drmbus.core.service;


import com.drmbus.innerapi.dto.CompanyDetailsDTO;

public interface CompanyDetailsService {
    CompanyDetailsDTO create(CompanyDetailsDTO companyDTO);
    CompanyDetailsDTO fetchByCompanyId(Long companyId);
    CompanyDetailsDTO update(CompanyDetailsDTO companyDTO);
}
