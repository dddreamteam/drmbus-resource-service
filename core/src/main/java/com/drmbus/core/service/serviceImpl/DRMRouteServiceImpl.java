package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.mapper.DRMRouteMapper;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.core.repository.DRMRouteRepository;
import com.drmbus.core.service.DRMRouteService;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/8/2020
 */

@Service
@Slf4j
public class DRMRouteServiceImpl implements DRMRouteService {

    private final DRMRouteRepository routeRepository;
    private final DRMRouteMapper routeMapper;
    private final CompanyRepository companyRepository;

    public DRMRouteServiceImpl(
            DRMRouteRepository routeRepository,
            DRMRouteMapper routeMapper,
            CompanyRepository companyRepository
    ) {
        this.routeRepository = routeRepository;
        this.routeMapper = routeMapper;
        this.companyRepository = companyRepository;
    }

    @Override
    public DRMRouteDTO create(DRMRouteDTO routeDTO) {
        DRMRoute DRMRoute = routeMapper.toDRMRoute(routeDTO);
        // Create DRMRoute in db
        DRMRoute route = routeRepository.saveAndFlush(DRMRoute);

        return routeMapper.toDRMRouteDTO(route);
    }

    @Override
    public DRMRouteDTO fetch(Long routeId) {
        Optional<DRMRoute> routeById = routeRepository.findById(routeId);
        if (routeById.isPresent()) {
            return routeMapper.toDRMRouteDTO(routeById.get());
        } else {
            log.error("Couldn't find DRMRoute with such ID in DB");
            throw new RuntimeException("Couldn't find DRMRoute with such ID in DB");
        }
    }

    @Override
    public DRMRouteDTO update(DRMRouteDTO routeDTO) {
        DRMRoute DRMRoute = routeMapper.toDRMRoute(routeDTO);
        // Update DRMRoute in db
        DRMRoute route = routeRepository.saveAndFlush(DRMRoute);

        return routeMapper.toDRMRouteDTO(route);
    }

    @Override
    public void delete(Long routeId) {
        routeRepository.deleteById(routeId);
    }


    @Override
    public List<DRMRouteDTO> fetchAllByCompanyId(Long companyId) {
        return routeRepository.findAllByCompany_Id(companyId).stream().map(
           routeMapper::toDRMRouteDTO
        ).collect(Collectors.toList());
    }

    @Override
    public List<DRMRouteDTO> fetchAllByUserId(UUID uuid) {
        List<Company> companies = companyRepository.findAllByOwnerUser_Uuid(uuid);
        return companies
                .stream()
                .map(Company::getRoutes)
                .flatMap(Collection::stream)
                .map(routeMapper::toDRMRouteDTO)
                .collect(Collectors.toList());
    }
}
