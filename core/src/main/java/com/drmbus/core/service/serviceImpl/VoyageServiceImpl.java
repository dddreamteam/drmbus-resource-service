package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.entity.Voyage;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.mapper.VoyageMapper;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.core.repository.VoyageRepository;
import com.drmbus.core.service.DRMRouteService;
import com.drmbus.core.service.VoyageService;
import com.drmbus.innerapi.dto.VoyageDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class VoyageServiceImpl implements VoyageService {

    private final VoyageRepository repository;
    private final VoyageMapper mapper;
    private final CompanyRepository companyRepository;

    public VoyageServiceImpl(VoyageRepository repository, VoyageMapper mapper, CompanyRepository companyRepository) {
        this.repository = repository;
        this.mapper = mapper;
        this.companyRepository = companyRepository;
    }

    @Override
    public VoyageDTO create(VoyageDTO voyageDTO) {
        Voyage voyage = repository.save(mapper.toVoyage(voyageDTO));
        return mapper.toVoyageDTO(voyage);
    }

    @Override
    public VoyageDTO fetch(Long id) {
        Optional<Voyage> voyageOptional = repository.findById(id);
        voyageOptional.orElseThrow(() -> {
            throw new RequestException("Voyage with id " + id + " doesn't exist in db");
        });
        return mapper.toVoyageDTO(voyageOptional.get());
    }

    @Override
    public VoyageDTO update(VoyageDTO voyageDTO) {
        Voyage voyage = repository.save(mapper.toVoyage(voyageDTO));
        return mapper.toVoyageDTO(voyage);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<VoyageDTO> findAllByUserId(UUID uuid) {

        /*List<CompanyDTO> companies = companyService.findAllByUserId(uuid);
        List<DRMRouteDTO> routes = new ArrayList<>();
        List<Voyage> voyages = new ArrayList<>();
        companies.forEach( company -> {
            routes.addAll(routeService.fetchAllByCompanyId(company.getId()));
        });

        routes.forEach(route -> {
            voyages.addAll(repository.findAllByRoute_Id(route.getId()));
        });
        return voyages.stream().map(mapper::toVoyageDTO).collect(Collectors.toList());*/

        List<Company> companies = companyRepository.findAllByOwnerUser_Uuid(uuid);
        return companies
                .stream()
                .map(Company::getRoutes)
                .flatMap(Collection::stream)
                .map(DRMRoute::getVoyages)
                .flatMap(Collection::stream)
                .map(mapper::toVoyageDTO)
                .collect(Collectors.toList());
    }
}
