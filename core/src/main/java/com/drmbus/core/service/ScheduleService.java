package com.drmbus.core.service;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/15/2020
 */

import com.drmbus.innerapi.dto.ScheduleDTO;

public interface ScheduleService extends CFUDServiceInterface<ScheduleDTO, Long> {
}
