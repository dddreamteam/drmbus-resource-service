package com.drmbus.core.service.serviceImpl;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/3/2020
 */

import com.drmbus.core.entity.Email;
import com.drmbus.core.mapper.EmailMapper;
import com.drmbus.core.repository.EmailRepository;
import com.drmbus.core.service.EmailService;
import com.drmbus.innerapi.dto.EmailDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final EmailRepository emailRepository;
    private final EmailMapper emailMapper;

    public EmailServiceImpl(EmailRepository emailRepository, EmailMapper emailMapper) {
        this.emailRepository = emailRepository;
        this.emailMapper = emailMapper;
    }

    @Override
    public EmailDTO create(EmailDTO emailDTO) {
        Email email = emailMapper.toEmail(emailDTO);
        Email emailSaved = emailRepository.saveAndFlush(email);
        return emailMapper.toEmailDTO(emailSaved);
    }

    @Override
    public EmailDTO fetch(Long emailId) {
        Optional<Email> emailById = emailRepository.findById(emailId);
        if (emailById.isPresent()) {
            return emailMapper.toEmailDTO(emailById.get());
        } else {
            log.error("Couldn't find Email with id - " + emailId + " in database");
            throw new RuntimeException("Couldn't find requested email in database");
        }
    }

    @Override
    public EmailDTO update(EmailDTO emailDTO) {
        Email email = emailMapper.toEmail(emailDTO);
        // update emain in db
        Email emailFromDB = emailRepository.saveAndFlush(email);

        return emailMapper.toEmailDTO(emailFromDB);
    }

    @Override
    public void delete(Long emailId) {
        emailRepository.deleteById(emailId);
    }
}
