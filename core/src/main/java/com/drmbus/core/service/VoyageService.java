package com.drmbus.core.service;

import com.drmbus.innerapi.dto.VoyageDTO;

import java.util.List;
import java.util.UUID;

public interface VoyageService extends CFUDServiceInterface<VoyageDTO, Long> {
    List<VoyageDTO> findAllByUserId(UUID id);
}
