package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.CompanyDetails;
import com.drmbus.core.entity.Email;
import com.drmbus.core.entity.PhoneNumber;
import com.drmbus.core.mapper.CompanyDetailsMapper;
import com.drmbus.core.mapper.EmailMapper;
import com.drmbus.core.mapper.PhoneNumberMapper;
import com.drmbus.core.repository.CompanyDetailsRepository;
import com.drmbus.core.service.AddressService;
import com.drmbus.core.service.CompanyDetailsService;
import com.drmbus.core.service.EmailService;
import com.drmbus.core.service.PhoneNumberService;
import com.drmbus.innerapi.dto.AddressDTO;
import com.drmbus.innerapi.dto.CompanyDetailsDTO;
import com.drmbus.innerapi.dto.EmailDTO;
import com.drmbus.innerapi.dto.PhoneNumberDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CompanyDetailsServiceImpl implements CompanyDetailsService {

    private final CompanyDetailsRepository repository;
    private final CompanyDetailsMapper mapper;
    private final PhoneNumberMapper phoneNumberMapper;
    private final AddressService addressService;
    private final PhoneNumberService phoneNumberService;
    private final EmailService emailService;
    private final EmailMapper emailMapper;

    public CompanyDetailsServiceImpl(CompanyDetailsRepository repository,
                                     CompanyDetailsMapper mapper,
                                     PhoneNumberMapper phoneNumberMapper,
                                     AddressService addressService,
                                     PhoneNumberService phoneNumberService,
                                     EmailService emailService,
                                     EmailMapper emailMapper) {
        this.repository = repository;
        this.mapper = mapper;
        this.phoneNumberMapper = phoneNumberMapper;
        this.addressService = addressService;
        this.phoneNumberService = phoneNumberService;
        this.emailService = emailService;

        this.emailMapper = emailMapper;
    }

    @Override
    public CompanyDetailsDTO create(CompanyDetailsDTO companyDetailsDTO) {

        // Create address if present
        if (companyDetailsDTO.getRealAddress().getAddressId() == null) {
            AddressDTO addressDTO = addressService.create(companyDetailsDTO.getRealAddress());
            companyDetailsDTO.setRealAddress(addressDTO);
        }

        CompanyDetails companyDetails = mapper.toCompanyDetails(companyDetailsDTO);
        // Create company details
        CompanyDetails cd = repository.saveAndFlush(companyDetails);


        List<PhoneNumberDTO> phoneNumbers = companyDetailsDTO.getPhoneNumbers();
        List<EmailDTO> emails = companyDetailsDTO.getEmails();

        final Long companyDetailsId = cd.getCompanyDetailsId();

        // IF list of phones not empty create phones and UPDATE company
        if (!phoneNumbers.isEmpty()) {

            List<PhoneNumber> collect = phoneNumbers.stream().map(phoneNumberDTO -> {
                phoneNumberDTO.setCompanyDetailsId(companyDetailsId);
                PhoneNumberDTO pnDTO = phoneNumberService.create(phoneNumberDTO);
                /*
                 * Fucking JPA does not understand that WE need fucking ID in PhoneNumber
                 * so we put it manually. It's because JPA could not see generated id in the same transaction.
                 * */
                return phoneNumberMapper.toPhoneNumber(pnDTO);
            }).collect(Collectors.toList());
            cd.setPhoneNumbers(collect);
        }

        if (!emails.isEmpty()) {
            List<Email> collect = emails.stream().map(emailDTO -> {
                emailDTO.setCompanyDetailsId(companyDetailsId);
                EmailDTO eDTO = emailService.create(emailDTO);
                return emailMapper.toEmail(eDTO);
            }).collect(Collectors.toList());
            cd.setEmails(collect);
        }
        return mapper.toCompanyDetailsDTO(cd);
    }

    @Override
    public CompanyDetailsDTO fetchByCompanyId(Long companyId) {
        Optional<CompanyDetails> optionalCompanyDetails = repository.findByCompany_Id(companyId);
        if (optionalCompanyDetails.isPresent()) {
            return mapper.toCompanyDetailsDTO(optionalCompanyDetails.get());
        } else {
            throw new RuntimeException("Couldn't find CompanyDetails in DB");
        }
    }

    @Override
    public CompanyDetailsDTO update(CompanyDetailsDTO companyDTO) {
        CompanyDetails companyDetails = mapper.toCompanyDetails(companyDTO);
        CompanyDetails cd = repository.saveAndFlush(companyDetails);
        return mapper.toCompanyDetailsDTO(cd);
    }
}
