package com.drmbus.core.service;

import com.drmbus.innerapi.dto.AddressDTO;

public interface AddressService extends CFUDServiceInterface<AddressDTO, Long> {
}
