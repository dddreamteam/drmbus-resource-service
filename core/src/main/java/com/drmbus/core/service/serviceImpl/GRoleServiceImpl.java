package com.drmbus.core.service.serviceImpl;

import com.drmbus.core.entity.GRole;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.GRoleRepository;
import com.drmbus.core.service.GRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class GRoleServiceImpl implements GRoleService {

    private final GRoleRepository gRoleRepository;

    public GRoleServiceImpl(GRoleRepository gRoleRepository) {
        this.gRoleRepository = gRoleRepository;
    }

    @Override
    public GRole create(GRole gRole) {
        return null;
    }

    @Override
    public GRole fetch(Long id) {
        try{
            Optional<GRole> role = gRoleRepository.findById(id);
            if(role.isPresent()){
                return role.get();
            } else {
                log.warn("Role with ID = " + id + " is not present");
                throw new RequestException("Role with ID = " + id + " is not present", HttpStatus.BAD_REQUEST);
            }
        }catch (DataAccessException e){
            log.warn("Database temporarily unavailable.");
            throw new RequestException("Service unavailable error.", HttpStatus.SERVICE_UNAVAILABLE);
        }

    }

    @Override
    public GRole update(GRole gRole) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
