package com.drmbus.core.mapper;


import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.CompanyDetails;
import com.drmbus.core.entity.User;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.CompanyDetailsRepository;
import com.drmbus.core.repository.UserRepository;
import com.drmbus.innerapi.dto.CompanyDTO;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class CompanyMapper {

    @Autowired
    private CompanyDetailsRepository companyDetailsRepository;

    @Autowired
    private UserRepository userRepository;


    @Mapping(target = "ownerUser",ignore = true)
    public abstract CompanyDTO toCompanyDTO(Company company);


    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "payDetails", ignore = true)
    @Mapping(target ="routes", ignore = true)
    @Mapping(target = "members", ignore = true)
    @Mapping(target = "companyDetails", ignore = true)
    @Mapping(target = "ownerUser", ignore = true)
    public abstract Company toCompany(CompanyDTO companyDTO);


    @AfterMapping
    protected void fillIgnoredFieldInToCompany(CompanyDTO comD, @MappingTarget Company com){
        Optional<CompanyDetails> optionalCompanyDetails = companyDetailsRepository.findByCompany_Id(comD.getId());
        if(optionalCompanyDetails.isPresent()){
            CompanyDetails companyDetails = optionalCompanyDetails.get();
            com.setCompanyDetails(companyDetails);
        } else {

        }

        Optional<User>  optionalUser = userRepository.findByUuid(comD.getOwnerUser());

        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            com.setOwnerUser(user);}
        else {
            System.out.println("no User");
            log.error("User with uuid = " + comD.getOwnerUser() + " not found in DB");
            throw new RequestException("User with uuid = " + comD.getOwnerUser()  + " not found in DB"); // TODO
        }
    }


    @AfterMapping
    protected  void fillIgnoredFieldInToCompanyDTO(Company com, @MappingTarget CompanyDTO comD){
        comD.setOwnerUser(com.getOwnerUser().getUuid());
    }
}
