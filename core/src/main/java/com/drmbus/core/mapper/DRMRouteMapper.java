package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/7/2020
 */

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.DRMRoute;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.innerapi.dto.DRMRouteDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Mapper(uses = StationMapper.class, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class DRMRouteMapper {

    @Autowired
    CompanyRepository companyRepository;

    @Mapping(target = "companyId", ignore = true)
    public abstract DRMRouteDTO toDRMRouteDTO(DRMRoute drmRoute);

    @Mapping(target = "company", ignore = true)
    @Mapping(target = "stations", source = "stations")
    @Mapping(target = "voyages", ignore = true)
    @Mapping(target = "schedule", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    public abstract DRMRoute toDRMRoute(DRMRouteDTO drmRouteDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToDRMRouteDTO(
            DRMRoute drmRoute,
            @MappingTarget DRMRouteDTO drmRouteDTO
    ) {
        drmRouteDTO.setCompanyId(drmRoute.getCompany().getId());
    }

    @AfterMapping
    protected void fillIgnoredFieldsInToDRMRoute(
            DRMRouteDTO drmRouteDTO,
            @MappingTarget DRMRoute drmRoute
    ) {
        Optional<Company> companyById = companyRepository.findById(drmRouteDTO.getCompanyId());
        if (companyById.isPresent()) {
            drmRoute.setCompany(companyById.get());
        } else {
            throw new RequestException("Company does not exist in DB");
        }
    }

}
