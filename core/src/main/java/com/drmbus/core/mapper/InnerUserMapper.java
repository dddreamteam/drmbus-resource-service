package com.drmbus.core.mapper;

import com.drmbus.core.entity.CompanyCRoleUser;
import com.drmbus.core.entity.User;
import com.drmbus.core.entity.UserDetails;
import com.drmbus.innerapi.dto.CompanyDTO;
import com.drmbus.innerapi.dto.InnerUserDTO;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class InnerUserMapper {

    @Mapping(target = "firstName", ignore = true)
    @Mapping(target = "lastName", ignore = true)
    @Mapping(target = "middleName", ignore = true)
    @Mapping(target = "avatarURL", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "companies", ignore = true)
    @Mapping(target = "companyIdToCRoleTypeMap", ignore = true)
    public abstract InnerUserDTO toInnerUserDTO(User user);

    // Fill ignored fields in User To InnerUserDTO
    @AfterMapping
    protected void fillIgnoredFieldsInUserToInnerUserDTO(User user, @MappingTarget InnerUserDTO userDTO) {
        UserDetails userDetails = user.getDetails();
        if (userDetails != null){
            userDTO.setFirstName(user.getDetails().getFirstName());
            userDTO.setLastName(user.getDetails().getLastName());
            userDTO.setMiddleName(user.getDetails().getMiddleName());
            userDTO.setAvatarURL(user.getDetails().getAvatarURL());
        } else {
            userDTO.setFirstName("");
            userDTO.setLastName("");
            userDTO.setMiddleName("");
            userDTO.setAvatarURL("");
        }
        // * Set<GRole> has been verified in GRoleService, it can't be null (99%)
        List<String> roleTypes = user.getGRoles().stream().map(role -> role.getType().toString()).collect(Collectors.toList());
        userDTO.setRoles(roleTypes);


        List<CompanyCRoleUser> companyCRoleUsers = user.getCompanyCRoleUsers();

        if(companyCRoleUsers != null && !companyCRoleUsers.isEmpty()){

            List<CompanyDTO> allCompaniesByUserID = companyCRoleUsers.stream().map(cru -> {
                CompanyDTO companyDTO = new CompanyDTO();
                companyDTO.setName(cru.getCompany().getName());
                companyDTO.setLegalForm(cru.getCompany().getLegalForm().toString());
                companyDTO.setId(cru.getCompany().getId());
                companyDTO.setEdrpo_inn(cru.getCompany().getEdrpo_inn());
                companyDTO.setOwnerUser(cru.getCompany().getOwnerUser().getUuid());
                return companyDTO;
            }).collect(Collectors.toList());
            userDTO.setCompanies(allCompaniesByUserID);

            Map<Long, String> companyIdToCRoleTypeMap = companyCRoleUsers.stream().collect(Collectors.toMap(
                    cru -> cru.getCompany().getId(),
                    cru -> cru.getCRole().getType()
            ));
            userDTO.setCompanyIdToCRoleTypeMap(companyIdToCRoleTypeMap);
        } else {
            userDTO.setCompanies(new ArrayList<>());
            userDTO.setCompanyIdToCRoleTypeMap(new HashMap<>());
        }
    }
}
