package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 4/30/2020
 */

import com.drmbus.core.entity.Address;
import com.drmbus.innerapi.dto.AddressDTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class AddressMapper {

    public abstract AddressDTO toAddressDTO(Address address);

    @Mapping(target = "station", ignore = true)
    @Mapping(target = "payDetails", ignore = true)
    @Mapping(target = "companyDetails", ignore = true)
    public abstract Address toAddress(AddressDTO addressDTO);

}
