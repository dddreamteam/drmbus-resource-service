package com.drmbus.core.mapper;

import com.drmbus.core.entity.Company;
import com.drmbus.core.entity.CompanyDetails;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.CompanyRepository;
import com.drmbus.innerapi.dto.CompanyDetailsDTO;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
@Mapper(uses = {AddressMapper.class}, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class CompanyDetailsMapper {

    @Autowired
    private CompanyRepository companyRepository;

    @Mapping(target = "companyId", ignore = true)
    public abstract CompanyDetailsDTO toCompanyDetailsDTO(CompanyDetails companyDetails);

    @Mapping(target = "company", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    public abstract CompanyDetails toCompanyDetails(CompanyDetailsDTO companyDetailsDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToCompanyDetailsDTO(CompanyDetails cd, @MappingTarget CompanyDetailsDTO cdd){
        cdd.setCompanyId(cd.getCompany().getId());
    }
    @AfterMapping
    protected void fillIgnoredFieldsInToCompanyDetails(CompanyDetailsDTO cdd, @MappingTarget CompanyDetails cd){
        Optional<Company> optional = companyRepository.findById(cdd.getCompanyId());
        if (optional.isPresent()){
            Company company = optional.get();
            cd.setCompany(company);
        }else {
            throw new RequestException("Company was not found in DB");
        }
    }
}
