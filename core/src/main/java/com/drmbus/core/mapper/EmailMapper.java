package com.drmbus.core.mapper;

import com.drmbus.core.entity.CompanyDetails;
import com.drmbus.core.entity.Email;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.CompanyDetailsRepository;
import com.drmbus.innerapi.dto.EmailDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Optional;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class EmailMapper {

    @Autowired
    private CompanyDetailsRepository companyDetailsRepository;

    @Mapping(target = "companyDetailsId", ignore = true)
    public abstract EmailDTO toEmailDTO(Email email);

    @Mapping(target = "details", ignore = true)
    public abstract Email toEmail(EmailDTO emailDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToEmail(EmailDTO emailDTO, @MappingTarget Email email) {
        if (emailDTO.getCompanyDetailsId() != null) {
            Optional<CompanyDetails> companyDetailsOptional = companyDetailsRepository.findById(emailDTO.getCompanyDetailsId());
            companyDetailsOptional.ifPresentOrElse(
                    email::setDetails,
                    () -> { throw new RequestException("Company details not found in DB"); }
            );
        }
    }
    @AfterMapping
    protected void fillIgnoredFieldsInToEmailDTO(Email email, @MappingTarget EmailDTO emailDTO) {
        if (email.getDetails() != null) {
            emailDTO.setCompanyDetailsId(email.getDetails().getCompanyDetailsId());
        }
    }

}