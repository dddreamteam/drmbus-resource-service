package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import com.drmbus.core.entity.Seat;
import com.drmbus.core.entity.Voyage;
import com.drmbus.core.repository.VoyageRepository;
import com.drmbus.core.utile.SeatStatus;
import com.drmbus.innerapi.dto.SeatDTO;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class SeatMapper {

    @Autowired
    private VoyageRepository voyageRepository;

    @Mapping(target = "voyageId", ignore = true)
    @Mapping(target = "status", ignore = true)
    public abstract SeatDTO toSeatDTO(Seat seat);

    @Mapping(target = "voyage", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    public abstract Seat toSeat(SeatDTO seatDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToSeatDTO(
            Seat seat,
            @MappingTarget SeatDTO seatDTO
    ){
        seatDTO.setStatus(seat.getStatus().toString());
        seatDTO.setVoyageId(seat.getVoyage().getId());
    }

    @AfterMapping
    protected void fillIgnoredFieldsInToSeat(
            SeatDTO seatDTO,
            @MappingTarget Seat seat
    ){
        seat.setStatus(SeatStatus.valueOf(seatDTO.getStatus()));
        Voyage voyage = voyageRepository.findById(seatDTO.getVoyageId()).orElseThrow(() -> {
            throw new RuntimeException("Voyage with Id " + seatDTO.getVoyageId() + " couldn't found in DB");
        });
        seat.setVoyage(voyage);
    }

}
