package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/7/2020
 */

import com.drmbus.core.entity.Schedule;
import com.drmbus.core.entity.Station;
import com.drmbus.core.utile.ScheduleType;
import com.drmbus.innerapi.dto.ScheduleDTO;
import com.drmbus.innerapi.dto.StationDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = DRMRouteMapper.class, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class ScheduleMapper {

    @Mapping(target = "routeDTO", source="route")
    @Mapping(target = "type", ignore = true)
    public abstract ScheduleDTO toScheduleDTO(Schedule schedule);

    @Mapping(target = "route", source = "routeDTO")
    @Mapping(target = "type", ignore = true)
    public abstract Schedule toSchedule(ScheduleDTO scheduleDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToScheduleDTO(
            Schedule schedule,
            @MappingTarget ScheduleDTO scheduleDTO
    ){
        scheduleDTO.setType(schedule.getType().toString());
    }

    @AfterMapping
    protected void fillIgnoredFieldsInToSchedule(
            ScheduleDTO scheduleDTO,
            @MappingTarget Schedule schedule
    ){
        schedule.setType(ScheduleType.valueOf(scheduleDTO.getType()));
    }
}
