package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 4/30/2020
 */

import com.drmbus.core.entity.CompanyDetails;
import com.drmbus.core.entity.PhoneNumber;
import com.drmbus.core.exception.RequestException;
import com.drmbus.core.repository.CompanyDetailsRepository;
import com.drmbus.innerapi.dto.PhoneNumberDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class PhoneNumberMapper {

    @Autowired
    private CompanyDetailsRepository companyDetailsRepository;

    @Mapping(target = "companyDetailsId", ignore = true)
    public abstract PhoneNumberDTO toPhoneNumberDTO(PhoneNumber phoneNumber);

    @Mapping(target = "details", ignore = true)
    public abstract PhoneNumber toPhoneNumber(PhoneNumberDTO phoneNumberDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToPhoneNumber(PhoneNumberDTO phoneNumberDTO, @MappingTarget PhoneNumber phoneNumber) {
        if (phoneNumberDTO.getCompanyDetailsId() != null) {
            Optional<CompanyDetails> companyDetailsOptional = companyDetailsRepository.findById(phoneNumberDTO.getCompanyDetailsId());
            companyDetailsOptional.ifPresentOrElse(
                    phoneNumber::setDetails,
                    () -> { throw new RequestException("Company details not found in DB"); }
            );
        }
    }

    @AfterMapping
    protected void fillIgnoredFieldsInToPhoneNumberDTO(PhoneNumber phoneNumber, @MappingTarget PhoneNumberDTO phoneNumberDTO) {
        if (phoneNumber.getDetails() != null) {
            phoneNumberDTO.setCompanyDetailsId(phoneNumber.getDetails().getCompanyDetailsId());
        }
    }

}
