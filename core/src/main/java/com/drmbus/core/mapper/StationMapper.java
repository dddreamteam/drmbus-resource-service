package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import com.drmbus.core.entity.Station;
import com.drmbus.innerapi.dto.StationDTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {AddressMapper.class}, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class StationMapper {

    public abstract StationDTO toStationDTO(Station station);

    @Mapping(target = "route", ignore = true)
    public abstract Station toStation(StationDTO stationDTO);

}
