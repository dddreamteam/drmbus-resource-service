package com.drmbus.core.mapper;

/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import com.drmbus.core.entity.Voyage;
import com.drmbus.core.repository.DRMRouteRepository;
import com.drmbus.core.utile.VoyageStatus;
import com.drmbus.innerapi.dto.VoyageDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(uses = {DRMRouteMapper.class, SeatMapper.class}, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class VoyageMapper {


    @Mapping(target = "status", ignore = true)
    public abstract VoyageDTO toVoyageDTO(Voyage voyage);

    @Mapping(target = "status", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    public abstract Voyage toVoyage(VoyageDTO voyageDTO);

    @AfterMapping
    protected void fillIgnoredFieldsInToVoyageDTO(
            Voyage voyage,
            @MappingTarget VoyageDTO voyageDTO
    ) {
        voyageDTO.setStatus(voyage.getStatus().toString());
    }

    @AfterMapping
    protected void fillIgnoredFieldsInToVoyage(
            VoyageDTO voyageDTO,
            @MappingTarget Voyage voyage
    ) {
        voyage.setStatus(VoyageStatus.valueOf(voyageDTO.getStatus()));
    }

}
