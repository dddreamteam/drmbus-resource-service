package com.drmbus.core.repository;

import com.drmbus.core.entity.CRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CRoleRepository extends JpaRepository<CRole, Long> {
}
