package com.drmbus.core.repository;

import com.drmbus.core.entity.Address;
import com.drmbus.core.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
