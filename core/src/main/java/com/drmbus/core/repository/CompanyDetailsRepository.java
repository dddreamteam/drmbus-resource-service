package com.drmbus.core.repository;

import com.drmbus.core.entity.CompanyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyDetailsRepository extends JpaRepository<CompanyDetails, Long> {
   // @Query("select * from company_details where ")
    Optional<CompanyDetails> findByCompany_Id(Long id);
}
