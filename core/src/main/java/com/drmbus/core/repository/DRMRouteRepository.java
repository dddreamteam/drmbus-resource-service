package com.drmbus.core.repository;

import com.drmbus.core.entity.DRMRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DRMRouteRepository extends JpaRepository<DRMRoute, Long> {
    List<DRMRoute> findAllByCompany_Id(Long companyId);
}
