package com.drmbus.core.repository;

import com.drmbus.core.entity.GRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GRoleRepository extends JpaRepository<GRole, Long> {
}
