package com.drmbus.core.repository;

import com.drmbus.core.entity.CompanyCRoleUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyCRoleUserRepository extends JpaRepository<CompanyCRoleUser, Long> {
}
