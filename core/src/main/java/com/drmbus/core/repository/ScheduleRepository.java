package com.drmbus.core.repository;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/15/2020
 */

import com.drmbus.core.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
}
