package com.drmbus.core.repository;
/*
 *  Created by Pavlo Syzonenko
 *  Date: 5/6/2020
 */

import com.drmbus.core.entity.Station;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StationRepository extends JpaRepository<Station, Long> {
}
